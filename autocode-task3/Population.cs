﻿namespace PadawansTask1
{
    using System;

    public static class Population
    {
        public static int GetYears(int initialPopulation, double percent, int visitors, int currentPopulation)
        {
            if (initialPopulation <= 0)
                throw new ArgumentException("Initial population cannot be less or equals zero.");

            if (currentPopulation <= 0)
                throw new ArgumentException("Current population cannot be less or equals zero.");

            var yearCount = 0;
            var currentValue = (double) initialPopulation;
            var percentMultiplier = percent / 100;

            while (currentValue < currentPopulation)
            {
                currentValue += visitors + currentValue * percentMultiplier;
                ++yearCount;
            }

            return yearCount;
        }
    }
}